//
//  FCCalendarVC.swift
//  CalendarTest1
//
//  Created by Vitaliy Medvedev on 22.09.20.
//

import UIKit
import FSCalendar

class FCCalendarVC: UIViewController {

    @IBOutlet weak var calendarView: FSCalendar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.calendarView.dataSource = self;
        self.calendarView.delegate = self;
        self.calendarView.scrollDirection = .vertical
        self.calendarView.placeholderType = .none
        self.calendarView.headerHeight = 20
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.calendarView.select(Date(), scrollToDate: true)
    }

}

extension FCCalendarVC: FSCalendarDelegate, FSCalendarDataSource {

}
