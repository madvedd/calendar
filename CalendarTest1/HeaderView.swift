//
//  HeaderView.swift
//  CalendarTest1
//
//  Created by Vitaliy Medvedev on 22.09.20.
//

import UIKit
import JTAppleCalendar

class HeaderView: JTACMonthReusableView {
    @IBOutlet weak var title: UILabel!
}
