//
//  CalendarVC.swift
//  CalendarTest1
//
//  Created by Vitaliy Medvedev on 22.09.20.
//

import UIKit
import JTAppleCalendar

class CalendarVC: UIViewController {

    @IBOutlet weak var calendarView: JTACMonthView!
    
    @IBAction func nextPressed(_ sender: Any) {        
        self.calendarView.scrollToDate(Date())
    }
    var numberOfRows = 7
    let formatter = DateFormatter()
    var testCalendar = Calendar.current
    var generateInDates: InDateCellGeneration = .forAllMonths
    var generateOutDates: OutDateCellGeneration = .tillEndOfGrid
    var prePostVisibility: ((CellState, CellView?)->())? = {state, cell in
        if state.dateBelongsTo == .thisMonth {
            cell?.isHidden = false
        } else {
            cell?.isHidden = true
        }
    }
    var hasStrictBoundaries = true
    let disabledColor = UIColor.lightGray
    let enabledColor = UIColor.blue
    var monthSize: MonthSize? = MonthSize(defaultSize: 50, months: [75: [.feb, .apr]])
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.calendarView.ibCalendarDelegate = self
        self.calendarView.ibCalendarDataSource = self
        self.calendarView.allowsMultipleSelection = true
        self.calendarView.scrollDirection = .vertical
        self.calendarView.scrollingMode = .stopAtEachSection
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.calendarView.reloadData()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func handleCellConfiguration(cell: JTACDayCell?, cellState: CellState) {
        cell?.layer.cornerRadius = 15
        handleCellSelection(view: cell, cellState: cellState)
        handleCellTextColor(view: cell, cellState: cellState)
        prePostVisibility?(cellState, cell as? CellView)
    }
    
    // Function to handle the text color of the calendar
    func handleCellTextColor(view: JTACDayCell?, cellState: CellState) {
        guard let myCustomCell = view as? CellView  else {
            return
        }
        
        if cellState.isSelected {
            myCustomCell.dayLabel.textColor = .white
        } else {
            if cellState.dateBelongsTo == .thisMonth {
                myCustomCell.dayLabel.textColor = .black
            } else {
                myCustomCell.dayLabel.textColor = .gray
            }
        }
    }

    // Function to handle the calendar selection
    func handleCellSelection(view: JTACDayCell?, cellState: CellState) {
        guard let myCustomCell = view as? CellView else {return }
        myCustomCell.selectedView.layer.cornerRadius =  13
        if cellState.isSelected {
            myCustomCell.tintColor = .white
            myCustomCell.selectedView.isHidden = false
        } else {
            myCustomCell.selectedView.isHidden = true
        }
    }
    
    
}

// MARK : JTAppleCalendarDelegate
extension CalendarVC: JTACMonthViewDelegate, JTACMonthViewDataSource {
    func configureCalendar(_ calendar: JTACMonthView) -> ConfigurationParameters {
        
        formatter.dateFormat = "yyyy MM dd"
        formatter.timeZone = testCalendar.timeZone
        formatter.locale = testCalendar.locale
        
        
        let startDate = formatter.date(from: "2020 01 01")!
        let endDate = formatter.date(from: "2020 12 01")!
        
        let parameters = ConfigurationParameters(startDate: startDate,
                                                 endDate: endDate,
                                                 numberOfRows: numberOfRows,
                                                 calendar: testCalendar,
                                                 generateInDates: generateInDates,
                                                 generateOutDates: generateOutDates,
                                                 firstDayOfWeek: .monday,
                                                 hasStrictBoundaries: hasStrictBoundaries)
        return parameters
    }
    
    func configureVisibleCell(myCustomCell: CellView, cellState: CellState, date: Date, indexPath: IndexPath) {
        var style =  date < Date() ? 2 : 0
        if testCalendar.isDateInToday(date) {
            myCustomCell.backgroundColor = .red
            style = 0
        } else {
            myCustomCell.backgroundColor = .white
        }
                
        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: cellState.text)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: style, range: NSMakeRange(0, attributeString.length))
        myCustomCell.dayLabel.attributedText = attributeString
            
        handleCellConfiguration(cell: myCustomCell, cellState: cellState)
        
        
        if cellState.text == "1" {
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM"
            let month = formatter.string(from: date)
            myCustomCell.monthLabel.text = "\(month) \(cellState.text)"
        } else {
            myCustomCell.monthLabel.text = ""
        }
    }
    
    func calendar(_ calendar: JTACMonthView, willDisplay cell: JTACDayCell, forItemAt date: Date, cellState: CellState, indexPath: IndexPath) {
        // This function should have the same code as the cellForItemAt function
        let myCustomCell = cell as! CellView
        configureVisibleCell(myCustomCell: myCustomCell, cellState: cellState, date: date, indexPath: indexPath)
    }
    
    func calendar(_ calendar: JTACMonthView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTACDayCell {
        let myCustomCell = calendar.dequeueReusableCell(withReuseIdentifier: "CellView", for: indexPath) as! CellView
        configureVisibleCell(myCustomCell: myCustomCell, cellState: cellState, date: date, indexPath: indexPath)
        return myCustomCell
    }
        

    func calendar(_ calendar: JTACMonthView, didDeselectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        if testCalendar.isDateInToday(date) || date > Date(){
            handleCellConfiguration(cell: cell, cellState: cellState)
        }
    }

    func calendar(_ calendar: JTACMonthView, didSelectDate date: Date, cell: JTACDayCell?, cellState: CellState, indexPath: IndexPath) {
        if testCalendar.isDateInToday(date) || date > Date(){
            handleCellConfiguration(cell: cell, cellState: cellState)
        }
    }
    
    func calendar(_ calendar: JTACMonthView, didScrollToDateSegmentWith visibleDates: DateSegmentInfo) {

    }
    
    func calendar(_ calendar: JTACMonthView, willScrollToDateSegmentWith visibleDates: DateSegmentInfo) {
        
    }
    
    func calendar(_ calendar: JTACMonthView, headerViewForDateRange range: (start: Date, end: Date), at indexPath: IndexPath) -> JTACMonthReusableView {
        let date = range.start
        formatter.dateFormat = "MMM"
        let header = calendar.dequeueReusableJTAppleSupplementaryView(withReuseIdentifier: "HeaderView", for: indexPath)
            (header as! HeaderView).title.text = formatter.string(from: date)
        return header
    }
    
    func sizeOfDecorationView(indexPath: IndexPath) -> CGRect {
        let stride = calendarView.frame.width * CGFloat(indexPath.section)
        return CGRect(x: stride + 5, y: 5, width: calendarView.frame.width - 10, height: calendarView.frame.height - 10)
    }
    
    func calendarSizeForMonths(_ calendar: JTACMonthView?) -> MonthSize? {
        return monthSize
    }
}
