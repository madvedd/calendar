//
//  CellView.swift
//  CalendarTest1
//
//  Created by Vitaliy Medvedev on 22.09.20.
//

import UIKit
import JTAppleCalendar

class CellView: JTACDayCell {
    @IBOutlet var selectedView: UIView!
    @IBOutlet var dayLabel: UILabel!
    @IBOutlet var monthLabel: UILabel!
}
